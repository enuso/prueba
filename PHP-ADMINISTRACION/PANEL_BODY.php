<body>
<nav class="menu">
  <input id="menu__toggle" type="checkbox" class='menu__toggle' />
  <label for="menu__toggle" class="menu__toggle-label">
    <svg preserveAspectRatio='xMinYMin' viewBox='0 0 24 24'>
      <path d='M3,6H21V8H3V6M3,11H21V13H3V11M3,16H21V18H3V16Z' />
    </svg>
    <svg preserveAspectRatio='xMinYMin' viewBox='0 0 24 24'>
      <path d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z" />
    </svg>
  </label>
  <ol class='menu__content'>

    <li class="menu-item">
      <a href="#0">USUARIOS</a>
      <ol class="sub-menu">
        <li class="menu-item"><a href="LISTAR_USUARIOS.php">Listar Usuarios</a></li>
        <li class="menu-item"><a href="LISTAR_MENSAJES.PHP">Mensajes</a></li>
        <li class="menu-item"><a href="ELIMINAR_USUARIO.php">Eliminar usuarios</a></li>
      </ol>
    </li>

    <li class="menu-item"><a href="#0">ADMINISTRADORES</a></li>
          <ol class="sub-menu">
        <li class="menu-item"><a href="LISTAR_ADMINISTRADOR.php">Listar Administradores</a></li>
        <li class="menu-item"><a href="CREAR_ADMINISTRADOR.PHP">Crear Administrador</a></li>
      </ol>
    </li>


    <li class="menu-item">
      <a href="#0">RECETAS</a>
      <ol class="sub-menu">
         <li class="menu-item"><a href="CREAR_RECETA.php">Crear Recetas</a></li>
        <li class="menu-item"><a href="LISTAR_RECETAS.PHP">Listar Recetas</a></li>
        <li class="menu-item"><a href="ELIMINAR_RECETAS.php">Eliminar recetas</a></li>
      </ol>
    </li>


  

    <li class="menu-item"><a href="cerrar_sesion.php">CERRAR SESION</a></li>
  </ol>
</nav>
</body>