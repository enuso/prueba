<?php
/*-----CONECTAMOS Y TRAEMOS LOS DATOS DEL FORMULARIO---------*/
	include 'conexion_formulario.php';

	$titulo =$_POST['titulo'];
	$categoria =$_POST['categoria'];
	$elaboracion=$_POST['elaboracion'];
	$ingrediente =$_POST['ingrediente'];
	$nombre_img = $_FILES['imagen']['name'];

	$extension = pathinfo($nombre_img, PATHINFO_EXTENSION);
	$tamano = $_FILES['imagen']['size'];
/*---------------------------------------*/
	$verificar_receta=mysqli_query($conexion, "SELECT * FROM recetas WHERE Nombre='$titulo' ");
	if(mysqli_num_rows($verificar_receta)>0){
		echo'
		<script>
			alert("Este titulo ya existe pruebe con uno diferente.");
		
		window.location="CREAR_RECETA.php";
	
		</script>';
		exit();
	}

	$directorio = '/AC.PRACTICA-PHP/PHP-ADMINISTRACION/Img/';
	move_uploaded_file($_FILES['imagen']['tmp_name'],$_SERVER['DOCUMENT_ROOT'].$directorio.$nombre_img);
	session_start();
	$_SESSION['url']= $directorio.$nombre_img;
	$ruta= $directorio.$nombre_img;
/*----------------------------------------*/
	$registroReceta = $conexion->prepare("INSERT INTO recetas(Nombre, Categoria, Descripcion,Foto)
	VALUES(?, ?, ?,?)");
	$registroReceta->bind_param('ssss',$titulo , $categoria, $elaboracion,$ruta);
	$registroReceta->execute();

	$idReceta =mysqli_insert_id($conexion);
	$ingredientes =explode(";", $ingrediente);
	foreach ($ingredientes as $key) {
		$registroIngrediente = $conexion->prepare("INSERT INTO ingredientes(Ingrediente,Id_recetas) VALUES(?,?)");
		$registroIngrediente->bind_param('si',$key,$idReceta);
		$registroIngrediente->execute();
	}	
/*----------------------------------------*/
	if($registroReceta){
		echo'
		<script>
		window.location="CREAR_RECETA.PHP";
		</script>';
	}else{
		echo'
		<script>
			alert("Intentelo de nuevo, usuario no almacenado");
			window.location="CREAR_RECETA.PHP";
		</script>
		';
	}
	mysql_close(conexion);
?>