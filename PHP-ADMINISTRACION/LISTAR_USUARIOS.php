<?php  
$conexion=mysqli_connect('localhost','root','','web_recetas');

session_start();
include("PANEL_BODY.php");
include("PANEL_HEAD.php");
?>

<style type="text/css">
	
table{

	margin: 0 auto;
	top:20%;
	width: 1400px;
	padding-top: 40px;
	color: white;
	position: relative;
	border: solid;
	border-radius: 15px;
}
.titulo{
	text-align: center;
	color: black;
	font-weight: bold;
	font-size: 40px;
}
tr{
	font-size: 25px;
	border: solid;
	}
td{
	width:500px;
	text-align: center;
}
.apartado{
	color: black;
	font-weight: bolder;
	font-size: 18px;
}
  
* {
	margin: 0;
	padding: 0;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

body {
	background: #e9e9e9;
	color: #2e2e2e;
	font-family: Arial, helvetica, sans-serif;
}

.contenedor {
	width: 90%;
	max-width: 1000px;
	margin: auto; 
}

h1 {
	margin: 20px 0;
	text-align: center;
	font-family: "Oswald", Arial, helvetica, sans-serif;
	color: #ce7f08;
	font-weight: normal;
}

.contenedor .articulos {
	margin-bottom: 20px;
}

.contenedor .articulos ul {
	list-style: none;
}

.contenedor .articulos ul li {
	/*background: #fff;*/
	padding:15px 10px;
	border-bottom: 1px solid #c8c8c8;
}
.paginacion{
	margin: 0 auto;
	top:20%;
	width: 1400px;
	padding-top: 40px;
	position: relative;
	margin-left: 45%;
	
	
}
.paginacion ul {
	list-style: none;
}

.paginacion ul li {
	display: inline-block;
	margin-right: 10px; 
}

.paginacion ul .active a {
	background: #ce7f08;
}

.paginacion ul .disabled {
	background: #c8c8c8;
	display: inline-block;
	font-family: "Oswald", Arial, helvetica, sans-serif;
	padding: 15px 25px;
	cursor:not-allowed;
	color:#fff;
	text-decoration: none;
}

.paginacion ul li a {
	display: inline-block;
	font-family: "Oswald", Arial, helvetica, sans-serif;
	padding: 15px 25px;
	background: #424242;
	color:#fff;
	text-decoration: none;
}

.paginacion ul li a:hover {
	text-decoration: none;
	background: #3299bb;
}
  

</style>



<table class="admin" >
	<tr>

		<td class="titulo" colspan="9">USUARIOS</td>
	</tr>
	<tr>
		<td>ID</td>
		<td>NOMBRE</td>
		<td>PRIMER APELLIDO</td>
		<td>SEGUNDO APELLIDO</td>
		<td>USUARIO</td>
		<td>TELEFONO</td>
	</tr>
	<?php 
	require("Listar_Usuarios(1).php");
	foreach ($result as $usuario) {
		print_r($usuario);
	 ?>

	<tr>

	<td><?php  echo $usuario['id']; ?></td>
	<td><?php  echo $usuario['Nombre']; ?></td>
	<td><?php  echo $usuario['Apellido1']; ?></td>
	<td><?php  echo $usuario['Apellido2']; ?></td>
	<td><?php  echo $usuario['Usuario']; ?></td>	
	<td><?php  echo $usuario['Telefono']; ?></td>
	</tr>
	<?php } ?>
</table>
<div class="paginacion">
			<ul>
				<!-- Establecemos cuando el boton de "Anterior" estara desabilitado -->
				<?php if($pagina == 1): ?>
					<li class="disabled">&laquo;</li>
				<?php else: ?>
					<li><a href="?pagina=<?php echo $pagina - 1 ?>">&laquo;</a></li>
				<?php endif; ?>

				<!-- Ejecutamos un ciclo para mostrar las paginas -->
				<?php 
					for($i = 1; $i <= $numeroPaginas; $i++){
						if ($pagina === $i) {
							echo "<li class='active'><a href='?pagina=$i'>$i</a></li>";
						} else {
							echo "<li><a href='?pagina=$i'>$i</a></li>";
						}
					}
				 ?>

				<!-- Establecemos cuando el boton de "Siguiente" estara desabilitado -->
				<?php if($pagina == $numeroPaginas): ?>
					<li class="disabled">&raquo;</li>
				<?php else: ?>
					<li><a href="?pagina=<?php echo $pagina + 1 ?>">&raquo;</a></li>
				<?php endif; ?>
					
			</ul>
		</div>


