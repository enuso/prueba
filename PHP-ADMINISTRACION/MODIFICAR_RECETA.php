<?php  
session_start();
include("PANEL_BODY.php");
include("PANEL_HEAD.php");
?>
<style type="text/css">
	@import url(https://fonts.googleapis.com/css?family=Sniglet|Raleway:900);


body, html{
	height: 100%;
	padding: 0;
	margin: 0;
	font-family: 'Sniglet', cursive;
}
h1{
	font-weight: normal;
	font-size: 4em;
	font-family: 'Raleway', sans-serif;
	margin: 0 auto;
	margin-top: 30px;
	width: 500px;
	color: #F90;
	text-align: center;

}

/* Animation webkit */
@-webkit-keyframes myfirst
{
	0% {margin-left: -235px}
	90% {margin-left: 100%;}
	100% {margin-left: 100%;}
}

/* Animation */
@keyframes myfirst
{
	0% {margin-left: -235px}
	70% {margin-left: 100%;}
	100% {margin-left: 100%;}
}






header{
	height: 160px;
	background: url('http://www.geertjanhendriks.nl/codepen/form/golf.png') repeat-x bottom;
}

#form{
	height: 100%;	
	overflow: hidden;
	position: relative;
	
}
form{
	margin: 0 auto;
	width: 500px;
	padding-top: 40px;
	color: white;
	position: relative;
	
	
}
label, input, textarea{
	display: block;	
}
input, textarea{
	width: 500px;	
	border: none;
	border-radius: 20px;
	outline: none;
	padding: 10px;
	font-family: 'Sniglet', cursive;
	font-size: 1em;
	color: #676767;
	transition: border 0.5s;
	-webkit-transition: border 0.5s;
	-moz-transition: border 0.5s;
	-o-transition: border 0.5s;
	border: solid 3px #98d4f3;	
	-webkit-box-sizing:border-box;
	-moz-box-sizing:border-box;
	box-sizing:border-box;
	
}
.insertar{

}
select{
	width: 500px;	
	border: none;
	border-radius: 20px;
	outline: none;
	padding: 10px;
	font-family: 'Sniglet', cursive;
	font-size: 1em;
	color: #676767;
	transition: border 0.5s;
	-webkit-transition: border 0.5s;
	-moz-transition: border 0.5s;
	-o-transition: border 0.5s;
	border: solid 3px #98d4f3;	
	-webkit-box-sizing:border-box;
	-moz-box-sizing:border-box;
	box-sizing:border-box;
	
}
input:focus, textarea:focus{
	border: solid 3px #77bde0;	
}

textarea{
	height: 100px;	
	resize: none; 
	overflow: auto;
}
input[type="submit"]{
	background-color: black;
	color: white;
	height: 50px;
	cursor: pointer;
	margin-top: 30px;
	font-size: 1.29em;
	font-family: 'Sniglet', cursive;
	-webkit-transition: background-color 0.5s;
	-moz-transition: background-color 0.5s;
	-o-transition: background-color 0.5s;
	transition: background-color 0.5s;
}
input[type="submit"]:hover{
	background-color: #e58f0e;
	
}
label{
	font-size: 1.5em;
	margin-top: 20px;
	padding-left: 20px;
}
.formgroup, .formgroup-active, .formgroup-error{
	background-repeat: no-repeat;
	background-position: right bottom;
	background-size: 10.5%;
	transition: background-image 0.7s;
	-webkit-transition: background-image 0.7s;
	-moz-transition: background-image 0.7s;
	-o-transition: background-image 0.7s;
	width: 566px;
	padding-top: 2px;
}

.formgroup{
	background-image: url('http://www.geertjanhendriks.nl/codepen/form/pixel.gif');	
}
.formgroup-active{
	background-image: url('http://www.geertjanhendriks.nl/codepen/form/octo.png');
}
.formgroup-error{
	background-image: url('http://www.geertjanhendriks.nl/codepen/form/octo-error.png');
	color: red;
}
</style>
<div id="form">

<form id="waterform" method="post" action="Actualizar_recetas.php" enctype="multipart/form-data">



<div class="formgroup" id="name-form">
    <label for="name">TITULO</label>
    <input type="text" id="name" name="titulo" value="<?php echo $_SESSION['Nombre']; ?>" required />

    <input type="hidden" id="name" name="id_receta" value="<?php echo $_SESSION['id']; ?>" required />
</div>

<div class="formgroup"  id="email-form">
    <label for="email">CATEGORIA</label>
  <input type="text" id="name" name="categoria" value="<?php echo $_SESSION['Categoria'] ?>" required />
</div>
<p>

<!--<img src="<?php echo $_SESSION['Foto'] ?>  ?>" alt="">-->
<input id="imagen" name="imagen" size="30" type="file"/><br>
<input type="text" name="nombre_img" id="" value="<?php echo $_SESSION['Foto'] ?>">

<div class="formgroup" id="email-form">
    <label for="email">INGREDIENTES</label>
    <input type="text" id="ingrediente" value="<?php echo $_SESSION['Ingredientes'] ?>" name="ingrediente" />
</div>
<div class="formgroup" id="message-form">
    <label for="message">ELABORACION</label>
    <input type="text" id="message" name="elaboracion" value="<?php echo $_SESSION['Descripcion'] ?>">
</div>
	<input type="submit" value="Actualizar receta" />
</form>
</div>