<?php
	session_start();
	include 'conexion_formulario.php';
	$id_receta= $_POST['id_receta'];
	$titulo= $_POST['titulo'];
	$categoria= $_POST['categoria'];
	$elaboracion= $_POST['elaboracion'];
	$ingrediente= $_POST['ingrediente'];
/*---------------------------------------*/
	$nombre_img = $_FILES['imagen']['name'];
	$extension = pathinfo($nombre_img, PATHINFO_EXTENSION);
	$tamano = $_FILES['imagen']['size'];
/*---------------------------------------*/

$directorio = '/AC.PRACTICA-PHP/PHP-ADMINISTRACION/Img/';
	move_uploaded_file($_FILES['imagen']['tmp_name'],$_SERVER['DOCUMENT_ROOT'].$directorio.$nombre_img);
	session_start();
	$_SESSION['url']= $directorio.$nombre_img;
	$ruta= $directorio.$nombre_img;
/*----------------------------------------*/

	 $actualizar_receta = $conexion->prepare("UPDATE recetas SET Nombre = ?, 
	 Categoria = ?,
	 Descripcion = ?,
	 Foto =?
	
		 WHERE id = ?");
    $actualizar_receta->bind_param('ssssi',$titulo,$categoria,$elaboracion,$nombre_img,$id_receta);
    $actualizar_receta->execute(); 
	
 		$eliminar_ingredientes = $conexion->prepare("DELETE FROM ingredientes WHERE Id_recetas = ?");
        $eliminar_ingredientes->bind_param('i', $id_receta);
        $eliminar_ingredientes->execute(); 
        $result=$eliminar_ingredientes->close();


	
		$ingredientes =explode(";", $ingrediente);
	foreach ($ingredientes as $key) {
		$registroIngrediente = $conexion->prepare("INSERT INTO ingredientes(Ingrediente,Id_recetas) VALUES(?,?)");
		$registroIngrediente->bind_param('si',$key,$id_receta);
		$registroIngrediente->execute();
	}	

	 	echo'
		<script>
		window.location="LISTAR_RECETAS.PHP";
		</script>';
	 
?>