-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-12-2021 a las 21:10:50
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `web_recetas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administradores`
--

CREATE TABLE `administradores` (
  `Id` int(11) NOT NULL,
  `Administrador` varchar(30) NOT NULL,
  `Contrasena` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `administradores`
--

INSERT INTO `administradores` (`Id`, `Administrador`, `Contrasena`) VALUES
(1, 'Alejandro', '123456'),
(3, 'Lara', '123456'),
(4, 'Adrian', '123456'),
(5, 'Yoel', '123456'),
(6, 'Elena', '123456'),
(7, 'Jose', '123456'),
(8, 'Sergio', '123456'),
(9, 'Joel', '123456'),
(10, 'Daniel', '123456'),
(11, 'Sofia', '123456'),
(12, 'Vicente', '123456');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario`
--

CREATE TABLE `comentario` (
  `id` int(11) NOT NULL,
  `id_receta` int(11) NOT NULL,
  `usuario` varchar(30) NOT NULL,
  `comentario` varchar(100) NOT NULL,
  `valoracion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingredientes`
--

CREATE TABLE `ingredientes` (
  `Id` int(11) NOT NULL,
  `Ingrediente` varchar(200) NOT NULL,
  `Id_recetas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ingredientes`
--

INSERT INTO `ingredientes` (`Id`, `Ingrediente`, `Id_recetas`) VALUES
(71, 'Arroz bomba', 31),
(72, ' Pollo de corral', 31),
(73, '  Conejo', 31),
(74, ' Aceite de oliva', 31),
(75, ' Azafran, Pimiento', 31),
(76, ' Sal, Tomate triturado', 31),
(77, 'Ajo', 32),
(78, ' Cebolla', 32),
(79, ' Pimiento verde', 32),
(80, ' Pimiento rojo', 32),
(81, ' Tomates', 32),
(82, ' Calabacín', 32),
(83, ' sal', 32),
(84, ' Pimienta negra', 32),
(85, ' Aceite de oliva', 32),
(96, '2Kg pulpo', 33),
(97, ' 1Kg patata', 33),
(98, ' pimentón', 33),
(99, ' aceite de oliva', 33),
(100, ' sal gruesa', 33),
(101, '2Kg pulpo', 34),
(102, ' 1Kg patata', 34),
(103, ' pimentón', 34),
(104, ' aceite de oliva', 34),
(105, ' sal gruesa', 34),
(106, '300g pan tipo candeal', 35),
(107, ' 100ml agua', 35),
(108, ' 150g chorizo', 35),
(109, '150g panceta', 35),
(110, '5 dientes de ajo', 35),
(111, ' pimenton dulce', 35),
(112, ' 4 Huevos', 35),
(113, ' aceite', 35),
(114, ' sal', 35),
(115, '4 patatas', 36),
(116, ' 1 chorizo fresco', 36),
(117, ' 1 pimiento de color', 36),
(118, ' 1 pimiento choricero', 36),
(119, '1 cebolla', 36),
(120, '10g pimenton', 36),
(121, '10ml aceite', 36),
(122, 'sal', 36),
(123, '8 patatas', 37),
(124, ' 1 pimiento rojo', 37),
(125, ' 4 pimientos verdes', 37),
(126, ' 2 calabacines', 37),
(127, ' 1 cebolla', 37),
(128, ' aceite', 37),
(129, ' pimienta', 37),
(130, ' sal', 37),
(131, '1 bote de cardo en conserva', 38),
(132, ' 1 puñado almendras curdas', 38),
(133, ' 1 puñado de piñones', 38),
(134, ' harina', 38),
(135, ' leche', 38),
(136, ' 2 ajos', 38),
(137, ' aceite', 38),
(138, ' sal', 38),
(139, '2 cebollas', 39),
(140, ' 1 paletilla de cerdo', 39),
(141, ' sal', 39),
(142, ' azucar', 39),
(143, ' pimenton', 39),
(144, ' chile seco', 39),
(145, ' pimienta negra', 39),
(146, ' semillas de mostaza', 39),
(147, ' ajo en polvo', 39),
(148, ' vinagre de manzana', 39),
(149, 'carrillera de cerdo', 40),
(150, ' zanahorias', 40),
(151, ' pimento verde', 40),
(152, ' cebolla', 40),
(153, ' vion tinto', 40),
(154, ' pimiento rojo', 40),
(155, ' caldo de carne', 40),
(156, ' concentrado de tomate', 40),
(157, ' sal', 40),
(158, ' pimienta', 40),
(159, ' aceite', 40),
(160, '2 picantones', 41),
(161, ' 2 cucharadas de curry', 41),
(162, ' 1 cucharada de aceite de oliva', 41),
(163, ' 2 cebolla moradas', 41),
(164, ' ciruelas', 41),
(165, ' 2 manzanas', 41),
(166, ' miel', 41),
(167, ' sal', 41),
(168, '8patats', 42),
(169, ' ajo', 42),
(170, ' pimiento verde', 42),
(171, ' pimiento rojo', 42),
(172, ' pimentón dulce', 42),
(173, ' sal', 42),
(174, ' mantequilla', 42),
(175, ' aceite de olviva', 42),
(176, ' pimienta negra', 42),
(177, '4 huesos de tenrera', 43),
(178, ' 1 hueso de jamón ', 43),
(179, ' 3 puerros ', 43),
(180, ' zanahorias', 43),
(181, ' tomate', 43),
(182, ' ajo', 43),
(183, ' cebolla', 43),
(184, ' laurel', 43),
(185, ' tomillo', 43),
(186, ' pimienta', 43),
(187, ' agua', 43),
(188, 'paletilla de cerdo', 44),
(189, ' cebolla', 44),
(190, ' ajo', 44),
(191, ' mango', 44),
(192, ' zumo de naranja', 44),
(193, ' pimentón dulce', 44),
(194, ' pimentón picante', 44),
(195, ' comino', 44),
(196, ' vinagre de manzana', 44),
(197, ' aceite', 44),
(198, ' sal', 44),
(199, '1l de leche entera', 45),
(200, ' arroz redondo', 45),
(201, ' rama de canela', 45),
(202, ' piel de naranja', 45),
(203, ' azucar', 45),
(204, ' mantequilla', 45),
(205, 'patatas', 46),
(206, ' huevos', 46),
(207, ' cebolla', 46),
(208, ' aceite de oliva ', 46),
(209, 'churros', 47),
(215, 'alas de pollo', 49),
(216, ' cuchara de ajo en polvo', 49),
(217, ' mostaza', 49),
(218, ' pimentón', 49),
(219, ' aceite de oliva', 49),
(220, 'Salmón sin piel', 50),
(221, ' zumo de limón', 50),
(222, ' oregano', 50),
(223, ' ajo picado', 50),
(224, ' sal', 50),
(225, 'chiles jalapeños', 51),
(226, ' queso crema', 51),
(227, ' lonchas de tocino ahumado', 51),
(233, 'coliflor', 52),
(234, ' huevo', 52),
(235, ' Sriracha', 52),
(236, ' harina', 52),
(237, ' pan molido', 52),
(238, 'alas de pollo', 48),
(239, ' cuchara de ajo en polvo', 48),
(240, ' mostaza', 48),
(241, ' pimentón', 48),
(242, ' aceite de oliva', 48);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajes`
--

CREATE TABLE `mensajes` (
  `id` int(11) NOT NULL,
  `Usuario` varchar(30) NOT NULL,
  `Asunto` varchar(50) NOT NULL,
  `Mensaje` varchar(600) NOT NULL,
  `Leido` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `mensajes`
--

INSERT INTO `mensajes` (`id`, `Usuario`, `Asunto`, `Mensaje`, `Leido`) VALUES
(14, 'enuso', 'Mensaje2', 'Mensaje2', 0),
(15, 'enuso', 'Mensaje3', 'Mensaje3', 0),
(16, 'enuso', 'Mensaje4', 'Mensaje4', 0),
(17, 'enuso', 'Mensaje5', 'Mensaje3', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recetas`
--

CREATE TABLE `recetas` (
  `id` int(11) NOT NULL,
  `Nombre` varchar(80) NOT NULL,
  `Categoria` varchar(80) NOT NULL,
  `Foto` text NOT NULL,
  `Descripcion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `recetas`
--

INSERT INTO `recetas` (`id`, `Nombre`, `Categoria`, `Foto`, `Descripcion`) VALUES
(31, 'Paella Valenciana', 'Tradicionales', '/AC.PRACTICA-PHP/PHP-ADMINISTRACION/Img/paella.jpg', 'Toda paella que se precie comienza por un buen sofrito. En una paella cuanto más grande mejor, se sofríe en abundante aceite el pollo, el conejo, las judías, las alcachofas y los caracoles (la que veis en la foto no tiene garrofó porque no es temporada y el congelado no es igual), sazonando con un poco de sal y pimentón hacia el final. Cuando esté bien dorado se añade el tomate triturado y se rehoga.\r\n\r\nCon el sofrito listo se debe de añadir el agua. Las proporciones dependen mucho del fuego, del calor que haga, del grado de humedad y de lo grande que sea la paella, pero para comenzar, una buena proporción es la de añadir tres veces el volumen de agua que de arroz, aunque es la experiencia la que os hará ajustar y perfeccionar estas cantidades, que acabaréis haciendo a ojo, como hicieron la tía y la madre de mi novia, que eran las encargadas de esta paella (a pesar de que la tradición marca que sea el hombre de la casa el que la prepare).\r\n\r\nEchamos ahora algunos troncos más al fuego para que suba de potencia y se haga bien el caldo durante 25 o 30 minutos. Es un buen momento de echar el azafrán o, en su defecto, el sazonador de paella (el más popular es \"el paellador), que lleva sal, ajo, colorante y un poco de azafrán. En una paella cuanto más grande mejor, se sofríe en abundante aceite el pollo, el conejo, las judías, las alcachofas y los caracoles (la que veis en la foto no tiene garrofó porque no es temporada y el congelado no es igual), sazonando con un poco de sal y pimentón hacia el final. Cuando esté bien dorado se añade el tomate triturado y se rehoga.'),
(32, 'Pisto Manchego', 'Tradicionales', '/AC.PRACTICA-PHP/PHP-ADMINISTRACION/Img/PistoManchego.jpg', 'Elaboración: Llevamos a ebullición agua para pelar los tomates. Retiramos la parte dura y hacemos un corte de cruz en la base. Los introducimos en el agua durante 15-20 segundos, sacamos y llevamos a bol con agua helada. Los pelamos y trituramos. Después pelamos y picamos finamente la cebolla y el ajo. Además, lavamos el resto de verduras y la troceamos en igual tamaño. Calentamos una buena cantidad de aceite en la cazuela y pochamos ajo y cebolla a fuego suave durante 15 minutos. Añadimos los pimientos y pochamos otros 15 minutos. Por último, añadimos calabacín y tomate, salpimentamos, tapamos, dejando pochar durante una hora y media. Por último, retiramos la tapa, subimos el fuego y cocecmos hasta que el agua del tomate se haya evaporado. Debe quedar jugoso pero sin restos de agua.'),
(34, 'Pulpo a feira', 'Tradicionales', '/AC.PRACTICA-PHP/PHP-ADMINISTRACION/Img/pulpo.jpg', 'Arrancamos calentando mucha agua en una olla grande y alta. Cuando esté bien caliente y a punto de hervir, sumergimos el pulpo entero tres veces, lo que se llama asustarlo. Después, lo dejamos en el agua de cocción. Cuando vuelva el hervor, comenzamos a contar el tiempo, unos treinta minutos por cada 1,8kg a 2kg de pulpo. Lo pinchamos en la parte más gruesa de los tentáculos para ver cómo está de duro. Estará listo cuando ofrezca una resistencia parecida a la patata cocida. Cuando estñe cocinado, lo sacamos del agua y dejamos reposar, aprovechando el agua para añadir la patata en rodajas que se cocerá durante 15 minutos. Pasado este tiempo, cortamos con tijera los tentáculos del pulpo en rodajas de un centímetro y lo servimos en un plato de madera, remojado con algo de agua de cocción, sobre la que ponemos una capa de patatas. Sobre ellas, el pulpo con la sal gorda y espolvoreamos con pimentón y rociamos con aceite.'),
(35, 'Migas al pastor', 'Tradicionales', '/AC.PRACTICA-PHP/PHP-ADMINISTRACION/Img/migas.jpg', 'Desmigamos el pan en trozos pequeños, extendemos en una fuente amplia y dejamos secar 24 horas. Al día siguiente salpicamos con el agua, mezclada con un pellizco de sal. Tapamos con un paño húmedo y dejamos reposar 10 minutos. Mientras tanto cortamos la panceta en dados y el chorizo en rodajas. Calentamos un par de cucharadas de aceite en una sartén y sofreímos ambos. Retiramos y reservamos. En la grasa que han soltado la panceta y el chorizo rehogamos los ajos, enteros y con piel, y las migas de pan humedecidas. Removemos sin parar durante 20 minutos a fuego medio-bajo, para que las migas se impregnen bien de la grasa y el aroma de los ajos al tiempo que se van secando. Añadimos la panceta y el chorizo, espolvoreamos con el pimentón, removemos e, inmediatamente, retiramos del fuego. Freímos los huevos y servimos las migas con ellos.'),
(36, 'Patatas a la riojana', 'Tradicionales', '/AC.PRACTICA-PHP/PHP-ADMINISTRACION/Img/patatas rioja.jpg', 'En una cazuela se pone un poco de aceite de oliva y se dora la cebolla cortada en cuadraditos pequeños y el pimiento partido a la mitad. Se añade el chorizo sin piel en rodajas y se rehoga ligeramente. Echamos las patatas peladas, lavadas y cascadas en trozo regulares, no muy grandes, le damos unas vueltas al conjunto y agregamos el pimentón para que se haga un poco pero evitando que se queme. Por último, colocamos en la cazuela los pimientos choriceros abiertos y limpios y cubrimos con agua. Llevamos a ebullición y en ese momento bajamos el fuego para que el guiso se haga lentamente. Cuando la patata esté casi lista salamos. Probamos el punto de sal en el caldo y rectificamos si fuese necesario. Si deseamos el caldo de las patatas más espeso debemos mover la cazuela de vez en cuando para ayudar a que las patatas suelten su fécula. De todos modos podemos aplastar con un tenedor algunas junto a un poco de líquido y añadirlas.'),
(37, 'Fritada aragonesa', 'Tradicionales', '/AC.PRACTICA-PHP/PHP-ADMINISTRACION/Img/fritada.jpg', 'Pon a calentar aceite de oliva en una sarten lo más grande y ancha que tengas. Cuando el aceite esté caliente, fríe la patata, hasta que quede bien hecha. Coloca un colador sobre un bol. Retira la patata con ayuda de una espumadera y colocalá sobhre este para escurrir el aceite sobrante, que podemos reintegrar a la sartén. Cuando la patata esté bien escurrida, colocala sobre una fuente, en la que iremos mezclando todas las verduras. Tras la patata, en el mismo aceite, fríe el calabacín, cortado en dados, y repite toda la operación. Cuando esté listo, escúrrelo en el colador, y añadelo a la fuente con las patatas. Ahora es el turno de cebollas y pimientos que se fríen juntos. Corta la cebolla en juliana y los pimientos en dados. Escúrrelos de nuevo y juntalos en la fuente con las patatas y los calabacines. Salpimenta las hortalizas, remueve bien la mezcla y sirve de inmediato.'),
(38, 'Cardo en salsa de almendras', 'Tradicionales', '/AC.PRACTICA-PHP/PHP-ADMINISTRACION/Img/cardo.jpg', 'Empezamos poniendo los cardos con el líquido que llevan en un cazo y lo calentamos. Trituramos las almendras y los piñones con un robot de cocina, si queréis que quede una textura más gruesa podéis hacerlo en un mortero. Laminamos los ajos, calentamos un poco de aceite en una cazuela y los doramos. Echamos 2 cucharadas de harina y rehogamos por un minuto. Incorporamos las almendras y los piñones, removemos y dejamos hacer un poco más. Añadimos un chorro de leche y también un poco del líquido de los cardos, en cantidad suficiente para poder remover bien el rehogado y dejamos que espese un poco. Ponemos los cardos escurridos en otra cazuela y echamos por encima la salsa de almendras. Con mucho cuidado, para que los cardos no se rompan ni se deshagan, removemos para que se impregnen bien de la salsa. Lo mejor es ir meneando la cazuela para que todo se vaya uniendo'),
(39, 'Pulle porck', 'Slow food', '/AC.PRACTICA-PHP/PHP-ADMINISTRACION/Img/pulle pork.jpg', 'Bridamos la paletilla de cerdo o le pedimos a nuestro carnicero que lo haga por nosotros. Preparamos la mezcla de especias mezclando la sal, el azúcar moreno, el pimentón dulce, el chile seco molido, la pimienta negra molida, las semillas de mostaza (en su defecto podemos usar una cucharada de mostaza Dijon), el ajo en polo, la salsa Worcester y 100 ml de vinagre de manzana. Pelamos y cortamos en ocho trozos las cebollas y las colocamos sobre la base de la Crock Pot, regamos con la coca cola. Embadurnamos el cerdo con la mezcla de especias cubriéndolo bien por todos lados. Lo colocamos sobre la cebolla y tapamos. Programamos a baja temperatura durante 12 horas. Transcurrido este tiempo, apagamos la Crock Pot y dejamos que el cerdo se atempere en su interior antes de retirarlo y deshilacharlo. Trituramos la salsa y pasamos por un colador. La calentamos a fuego suave y retiramos el exceso de grasa. Salpimentamos al gusto y ajustamos el espesor, bien reduciendo la salsa durante un tiempo o bien con maicena.'),
(40, 'Carrillera de cerdo al vino tinto', 'Slow food', '/AC.PRACTICA-PHP/PHP-ADMINISTRACION/Img/carrillera.jpg', ' Vertemos el vino en un cacito y lo hervimos a fuego medio hasta que el volumen se reduzca a la mitad. Tardaremos, aproximadamente, unos 30 minutos. Mientras tanto, limpiamos las carrilleras retirando, con un cuchillo afilado y mucho cuidado de no cortarse, la capa de grasa que recubre uno de sus lados. Salpimentamos cada carrillera por los dos lados. Calentamos un poco de aceite en una sartén y marcamos las carrilletas a fuego fuerte, por los dos lados. De esta manera los jugos quedan atrapados en su interior y el resultado, al final del proceso de elaboración, es una carrillera más tierna y jugosa. Lavamos las verduras bajo un chorro de agua fría. Pelamos las zanahorias y las cortados en discos. Pelamos la cebolla y la cortamos en tiras, así como los dos tipos de pimiento. Colocamos las verduras en la base de la Crock Pot y, sobre ellas, el concentrado de tomate. Salpimentamos al gusto pero con prudencia, pues podemos rectificar el punto más tarde. Colocamos las carrilleras sobre las verduras, regamos con el vino tinto reducido y el caldo de carne. Programamos la Crock Pot durante siete horas en posición baja. Retiramos las carrilleras y trituramos la salsa con una batidora o robot. Si queremos que quede más fina la pasamos por un chino, pero no es necesario. Ajustamos el punto de sal antes de servirla con las carrilleras.'),
(41, 'Picantones al curry con manzana', 'Slow food', '/AC.PRACTICA-PHP/PHP-ADMINISTRACION/Img/picantones.jpg', 'Marinamos los picantones para que la carne se impregne de sabor y aroma del curry. Mezclamos el aceite, el curry y una cucharadita de sal en un cuenco. Con ello cubrimos bien toda la superficie de los picantones. Los envolvemos individualmente en papel transparente y los dejamos marinar en la nevera durante 10-12 horas. Si lo hacemos por la noche, al día siguiente estarán perfectos. Retiramos los picantones de la nevera y los dejamos atemperar. Pelamos las cebollas, las cortamos en juliana y cubrimos la base de la cazuela de la Crock Pot con ellas. Pelamos las manzanas, las descorazonamos y las cortamos en gajos. Cubrimos la cebolla con la manzana y las ciruelas y sazonamos. Colocamos los picantones encima, pechugas hacia arriba, y regamos con la miel. Programamos la Crock Pot durante cuatro horas a temperatura baja. Si queremos dar un toque crujiente a la piel, retiramos los picantones una vez asados y los colocamos sobre una bandeja de horno forrada con papel de aluminio. La introducimos en el horno y gratinamos los picantones a 220ºC volteándolos para que se doren por todos lados. Servimos inmediatamente.'),
(42, 'Patatas asadas', 'Slow food', '/AC.PRACTICA-PHP/PHP-ADMINISTRACION/Img/patatas asadas.jpg', 'La mejor patata para esta receta es la patata nueva, de piel fina y textura dura y prienta, pues no la vamos a pelar para aprovechar bien sus nutrientes. Aunque esto es opcional, pero así os lo recomendamos. Lavamos las patatas y las secamos con papel de cocina absorbente. Cortamos en piezas de unos 3-4 cm de largo. Engrasamos la Crock Pot con un poco del aceite de oliva y metemos en ella las patatas. Lavamos los pimientos, los vaciamos de semillas y desechamos el pedúnculo. Cortamos en tiras y las añadimos a la patata. A continuación, pelamos y picamos los ajos y lo añadimos junto con el pimentón, la sal, la mantequilla en dados, el resto del aceite y unas vueltas de pimienta negra molida. Removemos bien para que se mezclen todos los sabores y las patatas se impregnen de las especias. Tapamos la Crock Pot y programamos tres horas a alta temperatura. El tiempo dependerá del tipo de patata que usemos y del tamaño de los trozos, así que comprobamos si están tiernas antes de apagar la olla y servir.'),
(43, 'Caldo de carne', 'Slow food', '/AC.PRACTICA-PHP/PHP-ADMINISTRACION/Img/caldo de carne.jpg', 'Lavamos los huesos bajo un chorro de agua fría para retirar posibles restos de sangre e impurezas. Los escurrimos y los colocamos sobre la base de la crock pot. Lavamos también las zanahorias, cortamos en discos y desechamos los extremos. Pelamos y cortamos la cebolla en trozos regulares. Pelamos el diente de ajo y cortamos el tomate. Separamos la parte verde de la blanca de los puerros y guardamos esta última para otras elaboraciones, solo vamos a usar las hojas verdes. Las lavamos bien procurando retirar los restos de arena que suelen contener, las cortamos y las añadimos a la olla junto con el resto de verduras, el laurel, el tomillo y la pimienta. Llenamos con agua suficiente para cubrir los huesos y verduras, tapamos y programamos 10 horas a baja temperatura. Transcurrido este tiempo, retiramos las verduras y colamos el caldo pasándolo a una olla. Introducimos en la nevera una vez atemperado y dejamos reposar un mínimo de tres horas. Retiramos la capa de grasa solidificada de la superficie y listo para usar.'),
(44, 'Paletilla de cerdo asada', 'Slow food', '/AC.PRACTICA-PHP/PHP-ADMINISTRACION/Img/paletilla de cerdo.jpg', 'Pelamos la cebolla y la cortamos en juliana. Pelamos y cortamos el diente de ajo en lonchas. Pelamos el mango, retiramos la carne del hueso y lo cortamos en dados. Exprimimos el zumo de tres naranjas y medimos la cantidad adecuada. Colocamos todos los ingredientes en la base del recipiente de nuestra Crock Pot. Preparamos una marinada con los dos tipos de pimentón, el comino molido, el vinagre, sal y suficiente aceite de oliva virgen extra como para crear una pasta. Untamos toda la superficie de la paletilla con la marinada y la colocamos en la Crock Pot. Regamos con el zumo de naranja y programamos siete horas de cocción a baja temperatura. Transcurrido el tiempo de cocción, dejamos enfriar la carne antes de retirar y cortar. La textura es tan tierna que se deshebrará quedando muy jugosa. Trituramos los ingredientes de la salsa, los pasamos por un chino y los dejamos reducir en un cacito a fuego lento, retirando la espuma que se forme en la superficie y ajustando el punto de sal. Servimos al gusto.\r\n\r\n'),
(45, 'Arroz con leche', 'Slow food', '/AC.PRACTICA-PHP/PHP-ADMINISTRACION/Img/arroz con leche.jpg', ' Retiramos la piel de la naranja o el limón, cualquiera que sea el cítrico elegido, sin tomar nada de la parte blanca. Esto es muy importante porque esta amarga mucho y estropearía el resultado final de nuestro arroz con leche. Lo mejor es usar un cuchillo muy afilado o un pelador e ir con sumo cuidado. No pasa nada si nos pasamos, retiramos lo blanco y punto. A continuación infusionamos la leche y, para ello, la vertemos en la olla junto con la canela y la piel del cítrico. Tapamos y programamos una hora en alta. Transcurrido este tiempo, añadimos el arroz (mejor si es de grano corto, queda más tierno y absorbe más cantidad de líquido). Removemos, tapamos de nuevo y programamos dos horas más en alta. Después de estas dos horas de cocción añadimos el azúcar y removemos para integrar. Añadimos más leche si lo vemos necesario (posiblemente no lo sea), tapamos y programamos una última hora en alta. Una vez listo, agregamos la mantequilla (opcional) y removemos bien. Podemos servir el arroz con leche templado o frío, espolvoreado con azúcar, caramelizada con soplete. '),
(46, 'Tortilla en feidora de aire', 'Sin aceite', '/AC.PRACTICA-PHP/PHP-ADMINISTRACION/Img/tortilla.jpg', 'Pelamos y troceamos las patatas y la cebolla en rodajas finas de 5 mm. Ponemos las patatas en la cesta de la freidora, salamos, añadimos el aceite Programamos 30 minutos a 180°C* (precalentado). Cuando hayan pasado 20 minutos incorporamos las cebollas. Removemos de vez en cuando para que se cocinen parejo. Pasado este tiempo controlamos que patatas y cebolla estén cocidas y añadimos el huevo batido y removemos para mezclar. Programamos 6-8 minutos a 130°C para cuajar la tortilla pero que quede jugosa.'),
(47, 'Churros sin aceite', 'Sin aceite', '/AC.PRACTICA-PHP/PHP-ADMINISTRACION/Img/churros.jpg', 'Precalentamos la freidora a 180°C, cuando esté caliente ponemos los churros que entren en la cesta. Puedes rociar la cesta con aceite si temes que se puedan pegar. Programamos 15 minutos* dándoles la vuelta a mitad de la cocción para que se doren parejos. Si pasado el tiempo todavía no están listos cocinamos unos minutos más. Al quitarlos los rebozamos con azúcar.'),
(48, 'Alitas de pollo', 'Sin aceite', '', 'Mezclamos la mostaza, el ajo, el pimentón y el aceite, ponemos las alitas dentro de este marinado, mezclamos bien y dejamos marinar 1 hora. Precalentamos la freidora a 180°C. Pulverizamos la cesta de la freidora con aceite, disponemos las alitas y programamos 20 minutos*. A mitad de la cocción agitamos la cesta para que las alitas no se peguen y se doren de manera uniforme. Si hace falta y te gustan más crujientes puedes dejarlas un poco más de tiempo.'),
(49, 'Alas de pollo', 'Sin aceite', '/AC.PRACTICA-PHP/PHP-ADMINISTRACION/Img/alas de pollo.jpg', 'Mezclamos la mostaza, el ajo, el pimentón y el aceite, ponemos las alitas dentro de este marinado, mezclamos bien y dejamos marinar 1 hora. Precalentamos la freidora a 180°C. Pulverizamos la cesta de la freidora con aceite, disponemos las alitas y programamos 20 minutos*. A mitad de la cocción agitamos la cesta para que las alitas no se peguen y se doren de manera uniforme. Si hace falta y te gustan más crujientes puedes dejarlas un poco más de tiempo.'),
(50, 'Salmón', 'Sin aceite', '/AC.PRACTICA-PHP/PHP-ADMINISTRACION/Img/salmon.jpg', 'Disponemos los filetes sobre una hoja de papel de aluminio, tiene que sobrar suficiente por los lados para poder envolver luego los filetes. Precalentamos la freidora a 180°C. Mezclamos el resto de los ingredientes, vertemos sobre los filetes de salmón y cerramos el papel de aluminio. Ponemos los paquetes de salmón en la cesta y programamos la freidora 10 minutos*. Retiramos la cesta, abrimos los paquetes y volvemos a cocinar 2 minutos más a 200°C para que el salmón se dore un poco. Servimos.'),
(51, 'Jalapeños rellenos de queso cubiertos con tocino', 'Sin aceite', '/AC.PRACTICA-PHP/PHP-ADMINISTRACION/Img/jalape.jpg', 'Disponemos los filetes sobre una hoja de papel de aluminio, tiene que sobrar suficiente por los lados para poder envolver luego los filetes. Precalentamos la freidora a 180°C. Mezclamos el resto de los ingredientes, vertemos sobre los filetes de salmón y cerramos el papel de aluminio. Ponemos los paquetes de salmón en la cesta y programamos la freidora 10 minutos*. Retiramos la cesta, abrimos los paquetes y volvemos a cocinar 2 minutos más a 200°C para que el salmón se dore un poco. Servimos.'),
(52, 'Coliflores empanizadas', 'Sin aceite', '', 'Precalentar la freidora de aire a 360°F o 180°F.Cortar en brotes pequeños la coliflor.Batir el huevo con la salsa Sriracha y agregar un poco de sal.Servir la harina en un plato y el pan molido en otro.Pasar cada flor por el huevo, la harina y el pan molido dos veces.Colocar en la canasta de la freidora de aire y cocinar por 15 minutos.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `Nombre` varchar(30) NOT NULL,
  `Apellido1` varchar(30) NOT NULL,
  `Apellido2` varchar(30) NOT NULL,
  `Usuario` varchar(15) NOT NULL,
  `Contrasena` varchar(15) NOT NULL,
  `Correo` varchar(11) NOT NULL,
  `Edad` int(11) NOT NULL,
  `Telefono` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `Nombre`, `Apellido1`, `Apellido2`, `Usuario`, `Contrasena`, `Correo`, `Edad`, `Telefono`) VALUES
(1, 'enuso', 'Usuario', 'Usuario', 'enuso', '123456', 'Usuario', 24, 'Usuario'),
(37, 'Lara', 'Lara', 'Lara', 'Lara', '123456', 'Lara', 1, 'Lara'),
(38, 'Adrian', 'Adrian', 'Adrian', 'Adrian', '123456', 'Adrian', 1, 'Adrian'),
(40, 'Daniel', 'Daniel', 'Daniel', 'Daniel', 'Daniel11', 'Daniel', 2, 'Daniel'),
(41, 'Joel', 'Joel', 'Joel', 'Joel', '123456', 'Joel', 1, 'Joel'),
(42, 'Nerea', 'Nerea', 'Nerea', 'Nerea', 'Nerea6', 'Nerea', 1, 'Nerea'),
(43, 'Ivan', 'Ivan', 'Ivan', 'Ivan', 'Ivan55', 'Ivan', 3, 'Ivan');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administradores`
--
ALTER TABLE `administradores`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `comentario`
--
ALTER TABLE `comentario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ingredientes`
--
ALTER TABLE `ingredientes`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `recetas`
--
ALTER TABLE `recetas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administradores`
--
ALTER TABLE `administradores`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `comentario`
--
ALTER TABLE `comentario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `ingredientes`
--
ALTER TABLE `ingredientes`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=243;

--
-- AUTO_INCREMENT de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `recetas`
--
ALTER TABLE `recetas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
