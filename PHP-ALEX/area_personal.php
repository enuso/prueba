<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login y Register</title>
    
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">

    <link rel="stylesheet" media="all" href="Boton.css" />
    <link rel="stylesheet" href="assets/css/estilos.css">
</head>
<style type="text/css">
    *{
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    text-decoration: none;
    font-family: 'Roboto', sans-serif;
}

body{
    background-image: url(assets/img/registro/fondo_area_personal.jpg);
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    background-attachment: fixed;
}

main{
    width: 100%;
    padding: 30px;
    margin: auto;
    margin-top: 100px;
}

.contenedor__todo{
    width: 100%;
    max-width: 1200px;
    margin: auto;
    position: relative;
}

.caja__trasera{
    width: 100%;
    padding: 10px 20px;
    display: flex;
    justify-content: center;
    -webkit-backdrop-filter: blur(10px);
    backdrop-filter: blur(10px);
    background-color: slategrey;

}

.caja__trasera div{
    margin: 100px 40px;
    color: white;
    transition: all 500ms;
}


.caja__trasera div p,
.caja__trasera button{
    margin-top: 30px;
}

.caja__trasera div h3{
    font-weight: 400;
    font-size: 26px;
}

.caja__trasera div p{
    font-size: 16px;
    font-weight: 300;
}

.caja__trasera button{
    padding: 10px 40px;
    border: 2px solid #fff;
    font-size: 14px;
    background: transparent;
    font-weight: 600;
    cursor: pointer;
    color: white;
    outline: none;
    transition: all 300ms;
}

.caja__trasera button:hover{
    background: #fff;
    color: #46A2FD;
}

/*Formularios*/

.contenedor__login-register{
    display: flex;
    align-items: center;
    width: 100%;
    max-width: 380px;
    position: relative;
    top: -185px;
    left: 10px;

    /*La transicion va despues del codigo JS*/
    transition: left 500ms cubic-bezier(0.175, 0.885, 0.320, 1.275);
}

.contenedor__login-register form{
    width: 100%;
    padding: 80px 20px;
    background: white;
    position: absolute;
    border-radius: 20px;
}

.contenedor__login-register form h2{
    font-size: 30px;
    text-align: center;
    margin-bottom: 20px;
    color: slategrey;
}

.contenedor__login-register form input{
    width: 100%;
    margin-top: 20px;
    padding: 10px;
    border: none;
    background: #F2F2F2;
    font-size: 16px;
    outline: none;
}

.contenedor__login-register form button{
    padding: 10px 40px;
    margin-top: 40px;
    border: none;
    font-size: 14px;
    background: slategrey;
    font-weight: 600;
    cursor: pointer;
    color: white;
    outline: none;
}




.formulario__login{
    opacity: 1;
    display: block;
}
.formulario__register{
    display: none;
    height: "100";
}
.formulario__eliminar{
    display: none;
    height: "100";
}



@media screen and (max-width: 850px){

    main{
        margin-top: 50px;
    }

    .caja__trasera{
        max-width: 350px;
        height: 300px;
        flex-direction: column;
        margin: auto;
    }

    .caja__trasera div{
        margin: 0px;
        position: absolute;
    }


    /*Formularios*/

    .contenedor__login-register{
        top: -10px;
        left: -5px;
        margin: auto;
    }

    .contenedor__login-register form{
        position: relative;
    }
}
</style>
<body>
    <?php
session_start();
?>
        <main>

            <div class="contenedor__todo">
                <div class="caja__trasera">
                    <div class="caja__trasera-login">
                        <h3>¿Quieres actualizar tu contraseña?</h3>
                        <button id="btn__iniciar-sesion">Actualizar</button>
                    </div>

                    <div class="caja__trasera-register">
                        <h3>¿Quieres modificar tus datos?</h3>
                        <button id="btn__registrarse">Modificar</button>
                    </div>

                    <div class="caja__trasera-eliminar">
                        <h3>¿Quieres eliminar tu perfil?</h3>
                        <button id="btn__iniciar-eliminar">Eliminar</button>
                    </div>
                </div>

                <!--Formulario de Login y registro-->
                <div class="contenedor__login-register">
                    <!--Login-->
                    <form action="actualizacion_contraseña.php" class="formulario__login" method="POST">
                        <h2>Actualizar Contaseña</h2>
                        <input type="text" placeholder="Usuario" name="usuario" required="">
                        <input type="password" placeholder="Contraseña"name="contrasena" required="">
                        <input type="password" placeholder="Nueva contraseña"name="contrasena1" required="" minlength="6">
                        <input type="password" placeholder="Repite la nueva contraseña"name="contrasena2" required="" minlength="6">
                        <button>Actualizar</button>
                    </form>

                    <!--Register-->
                    <form action="actualizar_datos_personales.php" method="POST" class="formulario__register" style="height:620px">
                        <h2>DATOS PERSONALES</h2>
        <input type="text" placeholder="Nombre" name="nombre" required="required" 
        value="<?php echo $_SESSION['nombre']; ?>" >

                         <input type="text" placeholder="Primer apellido" name="apellido1" required="required"
                          value="<?php echo $_SESSION['apellido1']; ?>">
                         <input type="text" placeholder="Segundo apellido" name= "apellido2" required="required"
                        value="<?php echo $_SESSION['apellido2']; ?>">
                        <input type="text" placeholder="Correo Electronico" name="correo" required="required"
                        value="<?php echo $_SESSION['correo']; ?>">
                        <input type="text" placeholder="Teléfono" name="telefono" required="required"
                        value="<?php echo $_SESSION['telefono']; ?>">
                        <input type="text" placeholder="Edad" name="edad" required="required"
                        value="<?php echo $_SESSION['edad']; ?>">
                        <button>Actualizar</button>
                    </form>

                    <form action="eliminar_usuario.php" class="formulario__eliminar" method="POST">
                        <h2>Eliminar perfil</h2>
                        <input type="password" placeholder="Contraseña"name="contrasena" required="">
                        <button>Eliminar</button>
                    </form>
            </div>

        </main>

 <script src="assets/js/script.js"></script>

</body>


<script type="text/javascript">
    //Ejecutando funciones
document.getElementById("btn__iniciar-sesion").addEventListener("click", iniciarSesion);
document.getElementById("btn__registrarse").addEventListener("click", register);
document.getElementById("btn__iniciar-eliminar").addEventListener("click", eliminar);
window.addEventListener("resize", anchoPage);

//Declarando variables
var formulario_login = document.querySelector(".formulario__login");
var formulario_register = document.querySelector(".formulario__register");
var formulario_eliminar = document.querySelector(".formulario__eliminar");

var contenedor_login_register = document.querySelector(".contenedor__login-register");

var caja_trasera_login = document.querySelector(".caja__trasera-login");
var caja_trasera_register = document.querySelector(".caja__trasera-register");
var caja_trasera_eliminar = document.querySelector(".caja__trasera-eliminar");

    //FUNCIONES

function anchoPage(){

    if (window.innerWidth > 850){
        caja_trasera_register.style.display = "block";
        caja_trasera_login.style.display = "block";
        caja_trasera_eliminar.style.display = "block";
    }else{
        caja_trasera_register.style.display = "block";
        caja_trasera_register.style.opacity = "1";

        
        caja_trasera_login.style.display = "none";
        formulario_login.style.display = "block";
        contenedor_login_register.style.left = "0px";

        caja_trasera_eliminar.style.display = "none";
        formulario_eliminar.style.display = "block";
       
        formulario_register.style.display = "none"; 
       
    }
}

anchoPage();


    function iniciarSesion(){
        if (window.innerWidth > 850){
            formulario_login.style.display = "block";
            contenedor_login_register.style.left = "0px";

            formulario_register.style.display = "none";
            formulario_eliminar.style.display = "none";
            caja_trasera_register.style.opacity = "1";
            caja_trasera_eliminar.style.opacity = "1";
            caja_trasera_login.style.opacity = "0";
        }else{
            alert("adioas");
            formulario_login.style.display = "block";
            contenedor_login_register.style.left = "0px";

            formulario_register.style.display = "none";
            caja_trasera_register.style.display = "block";
            formulario_eliminar.style.display = "none";
            caja_trasera_eliminar.style.display = "block";
            caja_trasera_login.style.display = "none";
        }
    }

    function register(){
        if (window.innerWidth > 850){
            formulario_register.style.display = "block";
            contenedor_login_register.style.left = "410px";


            formulario_login.style.display = "none";
            formulario_eliminar.style.display = "none";

            caja_trasera_register.style.opacity = "0";
            caja_trasera_login.style.opacity = "1";
            caja_trasera_eliminar.style.opacity = "1";
        }else{
            alert("adioas");
            formulario_register.style.display = "block";
            contenedor_login_register.style.left = "0px";

            formulario_login.style.display = "none";
            caja_trasera_register.style.display = "block";
            caja_trasera_eliminar.style.display = "none";

          
        }
}

function eliminar() {
        if (window.innerWidth > 850){

            formulario_eliminar.style.display = "block";
            contenedor_login_register.style.left = "800px";


            formulario_login.style.display = "none";
            formulario_register.style.display = "none";

            caja_trasera_eliminar.style.opacity = "0";
            caja_trasera_login.style.opacity = "1";
            caja_trasera_register.style.opacity = "1";
        }else{
            alert("adioas");

            formulario_eliminar.style.display = "block";
            contenedor_login_register.style.left = "0px";

            formulario_login.style.display = "none";
            formulario_register.style.display = "none";
            formulario_eliminar.style.display = "block";

        

            caja_trasera_eliminar.style.display = "none";

            caja_trasera_login.style.opacity = "0";
            caja_trasera_login.style.opacity = "1";

            caja_trasera_register.style.display = "block";
            caja_trasera_login.style.display = "block";

            caja_trasera_register.style.opacity = "0";
        }
}


</script>
</html>


  
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
</head>
<body>

</body>
</html>