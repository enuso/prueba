<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Recetas pal vicente</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
 <link rel="stylesheet" href="css/estilos.css">

    </head>

    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
            <div class="container">
                                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars ms-1"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav text-uppercase ms-auto py-4 py-lg-0">
                        <li class="nav-item"><a class="nav-link" href="#portfolio">Categorias</a></li>
                        <?php
        session_start();

        if (isset($_SESSION['usuario']) && isset($_SESSION['contrasena'])){
        ?>
        <li class="nav-item"><a class="nav-link" href="area_personal.php">Area personal</a></li>
        <li class="nav-item"><a class="nav-link" href="cerrar_sesion.php">Cerrar sesión</a></li>
        <li class="nav-item"><a class="nav-link" href="#contact">Contacto</a></li>
        <?php 
        }      
            ?>
                    </ul>

                </div>

            </div>

        </nav>

        <!-- Masthead-->
        <header class="masthead">
            <div class="container">
                <div class="masthead-subheading">RECETAS TRADICIONALES</div>
                <div class="masthead-heading text-uppercase"></div>
                <?php
                if (!isset($_SESSION['usuario']) && !isset($_SESSION['contrasena'])){

                    ?>
                <a class="btn btn-primary btn-xl text-uppercase" href=LOGIN+FORMULARIO.php>Acceso a usuarios</a>
                <?php
                    }
                    ?>

                     
            </div>
        </header>
        <!-- Services-->
 
        <!-- Portfolio Grid-->
        <section class="page-section bg-light" id="portfolio">

            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">LAS RECETAS MAS EXOTICAS AL ALCANCE DE UN CLICK</h2>
                    <h3 class="section-subheading text-muted">Selecciona tu receta preferida</h3>
                </div>
                <div class="row">
<!---------------------- LISTAR RECETAS------------------------------>

<table class="admin" >
    <tr>
        <td>NUMERO</td>
        <td>NOMBRE</td>
<hr size="8px" color="black" />
    </tr>
    <?php 
    require("listar_recetas_Slow_Food.php");
    foreach ($result as $usuario) {
      
     ?>

    <tr>
       <form action="Mostrar_receta.php" method="POST">
    <input type="hidden" name="id_receta" value="<?php  echo $usuario['id']; ?>">
   
    <td><?php  echo $usuario['id']; ?></td>
    <td><?php  echo $usuario['Nombre']; ?></td>
    <td><button type="submit">VER RECETA</button></td>
     </form>

    </tr>
         <?php } ?>
</table>
<div class="paginacion">
            <ul>
                <!-- Establecemos cuando el boton de "Anterior" estara desabilitado -->
                <?php if($pagina == 1): ?>
                    <li class="disabled">&laquo;</li>
                <?php else: ?>
                    <li><a href="?pagina=<?php echo $pagina - 1 ?>">&laquo;</a></li>
                <?php endif; ?>

                <!-- Ejecutamos un ciclo para mostrar las paginas -->
                <?php 
                    for($i = 1; $i <= $numeroPaginas; $i++){
                        if ($pagina === $i) {
                            echo "<li class='active'><a href='?pagina=$i'>$i</a></li>";
                        } else {
                            echo "<li><a href='?pagina=$i'>$i</a></li>";
                        }
                    }
                 ?>

                <!-- Establecemos cuando el boton de "Siguiente" estara desabilitado -->
                <?php if($pagina == $numeroPaginas): ?>
                    <li class="disabled">&raquo;</li>
                <?php else: ?>
                    <li><a href="?pagina=<?php echo $pagina + 1 ?>">&raquo;</a></li>
                <?php endif; ?>
                    
            </ul>
        </div>
<!---------------------- LISTAR RECETAS------------------------------>       
                    
                    <div class="col-lg-4 col-sm-6 mb-4 mb-lg-0"> 
        </section>
          
        <?php

        if (isset($_SESSION['usuario']) && isset($_SESSION['contrasena'])){
        ?>
       <section class="page-section" id="contact">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Contactanos</h2>
                    <p></p>
                    <h3 class="section-subheading text-muted">Si tienes algun problema, nosotros te lo solucionamos solo escribenos.</h3>
                </div>
                <!-- * * * * * * * * * * * * * * *-->
                <!-- * * SB Forms Contact Form * *-->
                <!-- * * * * * * * * * * * * * * *-->
                <!-- This form is pre-integrated with SB Forms.-->
                <!-- To make this form functional, sign up at-->
                <!-- https://startbootstrap.com/solution/contact-forms-->
                <!-- to get an API token!-->
                <form id="contactForm" data-sb-form-api-token="API_TOKEN" method="POST" action="MENSAJES_ADMIN.php">
                    <div class="row align-items-stretch mb-5">
                        <div class="col-md-6">
                            <div class="form-group">
                                <!-- Name input-->
                                <input class="form-control" id="name" type="text" placeholder="Asunto *" data-sb-validations="required" name="asunto" />
                                
                            
                               
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-group-textarea mb-md-0">
                                <!-- Message input-->
                                <textarea class="form-control" id="message" placeholder="Tu comentario *" data-sb-validations="required" name="mensaje" ></textarea>
                                
                            </div>
                        </div>
                    </div>
                    <!-- Submit success message-->
                    <!---->
                    <!-- This is what your users will see when the form-->
                    <!-- has successfully submitted-->
                    
                           

                    <!-- Submit Button-->
                    <div class="text-center"><button  class="boton_formul" >Enviar comentario</button></div>
                </form>
            </div>
        </section>
  

        <?php 
        }      
            ?>


        <!-- Contact-->
        
        <!-- Footer-->
        <footer class="footer py-4">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-4 text-lg-start">Copyright &copy; Your Website 2021</div>
                    <div class="col-lg-4 my-3 my-lg-0">
                        <a class="btn btn-dark btn-social mx-2" href="#!"><i class="fab fa-twitter"></i></a>
                        <a class="btn btn-dark btn-social mx-2" href="#!"><i class="fab fa-facebook-f"></i></a>
                        <a class="btn btn-dark btn-social mx-2" href="#!"><i class="fab fa-linkedin-in"></i></a>
                    </div>
                    <div class="col-lg-4 text-lg-end">
                        <a class="link-dark text-decoration-none me-3" href="#!">Privacy Policy</a>
                        <a class="link-dark text-decoration-none" href="#!">Terms of Use</a>
                    </div>
                </div>
            </div>
        </footer>
                  
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <!-- * *                               SB Forms JS                               * *-->
        <!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
    </body>
</html>
